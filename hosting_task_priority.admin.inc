<?php
/**
 * @file
 * Administrative functions for hosting task priority module.
 */

/**
 * Settings form for hosting task priority.
 */
function hosting_task_priority_settings_form($form, &$form_state) {
  $task_definitions = hosting_available_tasks();

  foreach ($task_definitions as $target => $tasks) {
    foreach ($tasks as $task_type => $task_info) {

      if (!isset($form[$target])) {
        $form[$target] = array(
          '#target' => $target,
          '#caption' => t('@target tasks', array('@target' => node_type_get_name($target))),
          '#pre_render' => array(
            'hosting_task_priority_settings_form_pre_render',
          ),
        );
      }

      $form[$target][$task_type]['title'] = array(
        '#markup' => isset($task_info['title']) ? $task_info['title'] : $task_type,
      );

      $form[$target][$task_type]['description'] = array(
        '#markup' => isset($task_info['description']) ? check_plain($task_info['description']) : '',
      );

      $form[$target][$task_type]['hosting_task_priority__' . $target . '__' . $task_type] = array(
        '#type' => 'radios',
        '#title' => t('Priority'),
        '#options' => hosting_task_priority_status_options(),
        '#default_value' => variable_get('hosting_task_priority__' . $target . '__' . $task_type, HOSTING_TASK_PRIORITY_NORMAL),
      );
    }
  }

  return system_settings_form($form);
}

/**
 * Pre render function for task priority tables.
 */
function hosting_task_priority_settings_form_pre_render($element) {
  $table = array(
    '#theme' => 'table',
    '#caption' => $element['#caption'],
    '#header' => array(
      t('Task'),
      t('Description'),
      array(
        'data' => t('Priority'),
        'colspan' => count(hosting_task_priority_status_options()),
      ),
    ),
    '#rows' => array(),
  );

  $target = $element['#target'];
  foreach (element_children($element) as $task_type) {
    $row = array(
      render($element[$task_type]['title']),
      render($element[$task_type]['description']),
    );
    foreach (hosting_task_priority_status_options() as $option_value => $option_title) {
      $row[] = render($element[$task_type]['hosting_task_priority__' . $target . '__' . $task_type][$option_value]);
    }

    $table['#rows'][] = $row;
  }

  return $table;
}
